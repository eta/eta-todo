(defpackage :eta-todo
  (:use :cl))

(in-package :eta-todo)

(setf (cl-who:html-mode) :html5)

(defvar *title* "eta-todo")
(defvar *todo-list* '())

;; Copied from TVL's panettone, with thanks to grfn
(defmacro render (() &body body)
  `(who:with-html-output-to-string (*standard-output* nil :prologue t)
     (:html
      :lang "en"
      (:head
       (:title (who:esc *title*))
       (:link :rel "stylesheet" :type "text/css" :href "//eta.st/assets/webfonts/stylesheet.css")
       (:link :rel "stylesheet" :type "text/css" :href "//eta.st/css/main.css")
       (:meta :name "viewport"
              :content "width=device-width,initial-scale=1"))
      (:header
       :class "site-header"
       (:div
        :class "wrapper"
        (:a
         :class "site-title"
         :href "/"
         (:img
          :src "//eta.st/assets/img/logo.svg"
          :alt "η")
         " (eta)")
        (:nav
         :class "site-nav"
         (:div
          :class "trigger"
          "eta-todo"))))
      (:div
       :class "page-content"
       (:div
        :class "wrapper"
        ,@body)))))

(defun save ()
  (with-open-file (f "./todos.lisp"
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    (prin1 *todo-list* f)))

(hunchentoot:define-easy-handler (deltodo :uri "/deltodo") ((id :parameter-type 'integer))
  (when (< id (length *todo-list*))
    (setf *todo-list* (append
                       (subseq *todo-list* 0 id)
                       (subseq *todo-list* (1+ id))))
    (save))
  (hunchentoot:redirect "/"))

(hunchentoot:define-easy-handler (newtodo :uri "/newtodo") ((todo :parameter-type 'string))
  (when todo
    (push todo *todo-list*)
    (save))
  (hunchentoot:redirect "/"))

(hunchentoot:define-easy-handler (metrics :uri "/metrics") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "# HELP eta_todo stuff eta has to do
# TYPE eta_todo gauge
~{eta_todo{thing=\"~A\"} 1~%~}" *todo-list*))

(hunchentoot:define-easy-handler (root :uri "/") ()
  (render ()
    (:h1
     "eta todo")
    (:p
     "stuff to do, I guess "
     (:a
      :href "/metrics"
      "(prometheus)"))
    (:form
     :action "/newtodo"
     :method "/post"
     (:input
      :type "text"
      :name "todo"
      :required)
     (:input
      :type "submit"
      :value "bonk"))
    (:ul
     (loop
       for i from 0
       for todo in *todo-list*
       do (who:htm
           (:li
            (who:esc todo)
            " &middot; "
            (:a
             :href (format nil "/deltodo?id=~A" i)
             "[done]")))))))

(defun load-todos ()
  (setf *todo-list* (read-from-string (uiop:read-file-string "./todos.lisp"))))

(defun main ()
  (load-todos)
  (hunchentoot:start
   (make-instance 'hunchentoot:easy-acceptor
		  :address "::"
                  :port 80))
  (loop (sleep 1)))

