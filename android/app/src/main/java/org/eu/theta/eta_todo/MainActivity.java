package org.eu.theta.eta_todo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private EditText todo_entry;
    private Button bonk;
    private OkHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        todo_entry = findViewById(R.id.todo_entry);
        bonk = findViewById(R.id.bonk);
        client = new OkHttpClient();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll()
                .build();

        StrictMode.setThreadPolicy(policy);
    }

    public void displayToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    public void onBonk(View view) {
        String todo = todo_entry.getText().toString();
        if (todo == "") {
            displayToast("can't be empty!");
            return;
        }

        submitTodo(todo);
    }

    private synchronized void submitTodo(String todo) {
        RequestBody body = new FormBody.Builder()
                .add("todo", todo)
                .build();
        Request request = new Request.Builder()
                .url("http://eta-todo.i.eta.st/newtodo")
                .post(body)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            displayToast("done");
            finish();
        } catch (Exception e) {
            displayToast("uh oh: " + e);
        }
    }
}